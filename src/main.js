import Vue from 'vue'
import App from './App.vue'
import VueMeta from 'vue-meta';
import VueAnalytics from 'vue-analytics';

Vue.use(VueAnalytics, {
    id: 'UA-128354060-6',
})
Vue.use(VueMeta);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
